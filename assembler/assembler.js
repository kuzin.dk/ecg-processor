/**
 * Run this script file with node.js
 */

const fs = require('fs');


// This function is borrowed from Pointy's answer at 
// https://stackoverflow.com/questions/10073699/pad-a-number-with-leading-zeros-in-javascript
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

const instructions = {
    "NOOP": { opcode: 0, immediate: false },
    "ADDI": { opcode: 1, immediate: true },
    "SUBI": { opcode: 2, immediate: true },
    "ADD": { opcode: 3, immediate: false },
    "SUB": { opcode: 4, immediate: false },
    "DIVC": { opcode: 5, immediate: true },
    "JNEG": { opcode: 6, immediate: true },
    "JEQ": { opcode: 7, immediate: true },
    "RMEM": { opcode: 8, immediate: false },
    "WMEM": { opcode: 9, immediate: false }
};


const instrRegex = /(\w{3,4})\s+(\d+)\s+(\d+)\s+(\w+)/;
const labelRegex = /(\w+):/;

function assemble(program) {
    let outputInstructions = [];

    const lines = program.split('\n');

    let currentLabel = { name: undefined };

    for (let i = 0; i < lines.length; i++) {
        const instrString = lines[i];
        
        const labelString = instrString.replace(/\/\/.*/, '');
        const labelMatch = labelRegex.exec(labelString);

        if (labelMatch && labelMatch.length > 0) {
            currentLabel.name = labelMatch[1];
            continue;
        }

        const instrObj = parseInstr(instrString, currentLabel, i);

        if (instrObj) {
            outputInstructions.push(instrObj);
        }
    }

    return outputInstructions.map((x, i) => `${i.toString(16)} ${getHexInstr(x, outputInstructions)}`).join("\n");
}

function parseInstr(instrString, currentLabel, i) {
    let instrObj;

    if (/NOOP/.test(instrString)) {
        // Opcode is NOOP
        instrObj = {
            instr: instructions.NOOP
        };
    } else {
        const matches = instrRegex.exec(instrString);

        if (!matches || matches.length === 0) {
            console.log("Skipped line " + (i + 1) + " - Instruction format was incorrect: " + instrString);
            return undefined;
        }

        const instr = instructions[matches[1]];

        if (!instr) {
            console.log("Unrecognized operation " + matches[1]);
            return undefined;
        }

        instrObj = {
            instr: instr,
            opr1: matches[2],
            opr2: matches[3],
            opr3: matches[4]
        };
    }

    if (currentLabel.name) {
        instrObj.label = currentLabel.name;
        currentLabel.name = undefined;
    }

    return instrObj;
}

function getHexInstr(instrObj, instrs) {
    return pad(parseInt(getBinInstr(instrObj, instrs), 2).toString(16), 8);
}

function getBinInstr(instrObj, instrs) {
    if (instrObj.instr === instructions.NOOP) {
        return "00000000000000000000000000000000";
    }

    instrObj.opr1 = parseInt(instrObj.opr1);
    instrObj.opr2 = parseInt(instrObj.opr2);

    if ((instrObj.instr === instructions.JEQ || instrObj.instr === instructions.JNEG) && /[A-Za-z]/.test(instrObj.opr3)) {
        // Operand 3 is probably a reference to a label
        const labelInstr = instrs.find(i => i.label && i.label === instrObj.opr3);

        if (!labelInstr) {
            throw `Invalid reference to label: "${instrObj.opr3}" on line ${instrs.indexOf(instrObj)}`;
        }

        instrObj.opr3 = instrs.indexOf(labelInstr);
    } else {
        instrObj.opr3 = parseInt(instrObj.opr3);
    }

    let result;

    if (!instrObj.instr.immediate) {
        result = pad(instrObj.instr.opcode.toString(2), 4) +
            pad(instrObj.opr1.toString(2), 3) +
            pad(instrObj.opr2.toString(2), 3) +
            pad(instrObj.opr3.toString(2), 3) +
            "0000000000000000000";
    } else {
        result = pad(instrObj.instr.opcode.toString(2), 4) +
            pad(instrObj.opr1.toString(2), 3) +
            pad(instrObj.opr2.toString(2), 3) +
            "000000" +
            pad(instrObj.opr3.toString(2), 16);
    }

    return result;
}

function replaceExtension(path) {
    const dotIndex = path.lastIndexOf('.');
    let newPath = path;

    if (dotIndex !== -1) {
        newPath = newPath.substring(0, dotIndex);
    }

    return newPath + ".bin";
}


fs.readFile(process.argv[2], 'ascii', function (err, data) {
    if (err) throw err;

    let newFilename = replaceExtension(process.argv[2]);

    fs.writeFile(newFilename, assemble(data), function (err) {
        if (err) throw err;
        console.log("Saved program to file '" + newFilename + "'");
    });
});