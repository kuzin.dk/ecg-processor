/**
 * Run this script file from the project directory with node.js on WSL Ubuntu with compiled FDLSIM installation at
 * /opt/gezel-sources/gezel-sim/build/bin/fdlsim
 */

const { spawnSync } = require( 'child_process' );
const fs = require('fs');

// This function is borrowed from Pointy's answer at 
// https://stackoverflow.com/questions/10073699/pad-a-number-with-leading-zeros-in-javascript
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

const instructions = {
    "NOOP": { opcode: 0, immediate: false },
    "ADDI": { opcode: 1, immediate: true },
    "SUBI": { opcode: 2, immediate: true },
    "ADD ": { opcode: 3, immediate: false },
    "SUB ": { opcode: 4, immediate: false },
    "DIVC": { opcode: 5, immediate: true },
    "JNEG": { opcode: 6, immediate: true },
    "JEQ ": { opcode: 7, immediate: true },
    "RMEM": { opcode: 8, immediate: false },
    "WMEM": { opcode: 9, immediate: false },
    "OP15": { opcode: 15, immediate: false }
};

const instrRegex = /CPU        - raw instr  : (\d{32})/;
const aluOutputRegex = /CPU        - alu output : (-?\d+), N: \d, Z: \d/;
const lineNumberRegex = /CPU        - line_number: (\d+)/;

const programDir = './programs/moving_window.txt';

// Assemble the source
const assemble = spawnSync( 'node', [ './assembler/assembler.js', programDir ] );

if(assemble.stderr.toString()) {
    throw assemble.stderr.toString();
}

console.log(assemble.stdout.toString());

// Replace program.txt
fs.copyFileSync(replaceExtension(programDir), './cpu/program.txt');
console.log('"program.txt" overwritten');

// Execute the program
const exec = spawnSync( '/opt/gezel-sources/gezel-sim/build/bin/fdlsim', [ 'cpu_platform.fdl', '10000' ], {cwd: './cpu'} );

if(exec.stderr.toString()) {
    throw exec.stderr.toString();
} else {
    // Write the parsed output to output.txt
    fs.writeFileSync('output.txt', parseOutput(exec.stdout.toString()));
    console.log('Saved output to "output.txt"');
}

function parseOutput(output) {
    let outputArr = [];
    let lines = output.split(/~~~~~~~~~~~~~~~~ Cycle \d+ ~~~~~~~~~~~~~~~~/);

    let cycleCount = 0;

    for (let i = 0; i < lines.length; i++) {
        const outputString = lines[i];

        if (!outputString.startsWith("\nCPU        - line_number")) {
            continue;
        }

        cycleCount++;

        const instr = parseInstr(outputString);
        const aluOutput = parseAluOutput(outputString);
        const lineNumber = parseLineNumber(outputString);

        if(lineNumber !== '4') {
            continue;
        }

        // if(lineNumber !== '3' && lineNumber !== '8') {
        //     continue;
        // }

        if (instr.startsWith('NOOP')) {
            continue;
        }

        
        // outputArr.push(`~~~~~~~~~~~~~~~~~~~~ Cycle ${cycleCount} ~~~~~~~~~~~~~~~~~~~\n\nLine Num    ${lineNumber}\n${instr}\n${aluOutput}\n`);
        outputArr.push(`Cycle ${pad(cycleCount, 4)} - Line Num ${pad(lineNumber, 2)} - ${instr}: ${aluOutput}`);
    }

    return outputArr.join('\n');
}

function parseAluOutput(outputString) {
    const aluOutputMatch = aluOutputRegex.exec(outputString);

    if (aluOutputMatch && aluOutputMatch.length > 0) {
        return 'ALU Output  ' + aluOutputMatch[1];
    }

    return 'Could not parse ALU Output';
}

function parseLineNumber(outputString) {
    const lineNumberMatch = lineNumberRegex.exec(outputString);

    if (lineNumberMatch && lineNumberMatch.length > 0) {
        return lineNumberMatch[1];
    }

    return 'Could not parse Line Number';
}

function parseInstr(outputString) {
    const instrMatch = instrRegex.exec(outputString);

    if (instrMatch && instrMatch.length > 0) {
        const instrString = instrMatch[1];

        const opcode = parseInt(instrString.substring(0, 4), 2);
        const instr = Object.entries(instructions).find(x => x[1].opcode == opcode);

        if (instr[0] === 'NOOP') {
            return 'NOOP'
        }

        let opr1 = instrString.substring(4, 7);
        let opr2 = instrString.substring(7, 10);

        opr1 = parseInt(opr1, 2);
        opr2 = parseInt(opr2, 2);

        let opr3;

        if (instr[1].immediate) {
            opr3 = instrString.substring(16, 32);
            opr3 = parseInt(opr3, 2);
            return `${instr[0]}        Register ${opr1}   Register ${opr2}   Operand: ${opr3}`;
        } else {
            opr3 = instrString.substring(10, 13);
            opr3 = parseInt(opr3, 2);
            return `${instr[0]}        Register ${opr1}   Register ${opr2}   Register ${opr3}`;
        }
    }

    return 'Could not parse instruction...';
}

function replaceExtension(path) {
    const dotIndex = path.lastIndexOf('.');
    let newPath = path;

    if (dotIndex !== -1) {
        newPath = newPath.substring(0, dotIndex);
    }

    return newPath + ".bin";
}